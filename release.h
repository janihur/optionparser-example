#ifndef _RELEASE_H_
#define _RELEASE_H_

#include <string>

#include "to_string.h"

const long RELEASE_NUMBER_MAJOR = 0;
const long RELEASE_NUMBER_MINOR = 1;

const std::string FULL_RELEASE_STRING = 
  "release-" +
  to_string<long>(RELEASE_NUMBER_MAJOR) + 
  "." +
  to_string<long>(RELEASE_NUMBER_MINOR);

#endif
