#include <cstdlib>
#include <iostream>

#include "optionparser.h"
#include "release.h"

enum optionIndex { UNKNOWN, HELP, VERSION, NONINTERACTIVE, TRACKMODE, 
		   REPLAYMODE };
const option::Descriptor usage[] = {
  {
    UNKNOWN, 0, "", "", option::Arg::None, 
    "USAGE: optionparser-example [OPTION]... CONFFILE\n"
    "\n"
    "CONFFILE is the configuration file\n"
    "\n"
    "OPTIONS" 
  },
  {
    HELP, 0, "h", "help", option::Arg::None, 
    "  -h, --help \tPrint usage and exit." 
  },
  {
    VERSION, 0, "v", "version", option::Arg::None, 
    "  -v, --version \tPrint version and exit." 
  },
  {
    NONINTERACTIVE, 0, "ni", "noninteractive", option::Arg::None,
    "  -ni, --noninteractive \tRun non-interactively."
  },
  {
    TRACKMODE, 0, "t", "track", option::Arg::None,
    "  -t, --track \tRun in track mode."
  },
  {
    REPLAYMODE, 0, "r", "replay", option::Arg::None,
    "  -r, --replay \tRun in replay mode."
  },
  {
    UNKNOWN, 0, "" , "", option::Arg::None,
    "\n"
    "EXAMPLES\n"
    "  optionparser-example --noninteractive --track default.ini\n"
    "  optionparser-example --replay default.ini\n"
  },
  { 0, 0, 0, 0, 0, 0 }
};

enum Mode_t { MISSING, ERROR, TRACKING, REPLAY };

Mode_t resolve_mode(const option::Option* const options) {
  if (options[TRACKMODE] and not options[REPLAYMODE]) {
    return TRACKING;
  } else if (options[REPLAYMODE] and not options[TRACKMODE]) {
    return REPLAY;
  } else if (options[TRACKMODE] and options[REPLAYMODE]) {
    return ERROR;
  }
  return MISSING;
}

int main(int argc, char* argv[]) {

  // skip program name argv[0] if present
  argc -= (argc > 0); argv += (argc > 0);
  option::Stats stats(usage, argc, argv);
  option::Option options[stats.options_max];
  option::Option buffer[stats.buffer_max];
  option::Parser parse(usage, argc, argv, options, buffer);

  if (parse.error()) {
    std::cout << "Failed to parse command line options. Aborted." << std::endl;
    return EXIT_FAILURE;
  }

  if (options[UNKNOWN].count() > 0) {
    std::cout << "Unknown option(s):";
    for (option::Option* opt = options[UNKNOWN]; opt; opt = opt->next())
      std::cout << " " << opt->name;
    std::cout << ". Aborted." << std::endl;
    return EXIT_FAILURE;
  }

  if (options[HELP] or argc == 0) {
    option::printUsage(std::cout, usage);
    return EXIT_SUCCESS;
  }

  const bool version = options[VERSION];

  if (version) {
    std::cout << FULL_RELEASE_STRING << std::endl;
    return EXIT_SUCCESS;
  }

  if (parse.nonOptionsCount() == 0) {
    std::cout << "Configuration file must be given. Aborted." << std::endl;
    return EXIT_FAILURE;
  }

  const bool interactive = not options[NONINTERACTIVE];

  if (interactive) {
    std::cout << "Running interactively." << std::endl;
  } else {
    std::cout << "Running non-interactively." << std::endl;
  }

  const Mode_t mode = resolve_mode(options);

  switch (mode) {
  case MISSING:
    std::cout << "One of --track, --replay options must be given. Aborted."
              << std::endl;
    break;
  case ERROR:
    std::cout << "Only one of --track, --replay options can be given. Aborted."
              << std::endl;
    break;
  case TRACKING:
    std::cout << "Running in track mode." << std::endl;
    break;
  case REPLAY:
    std::cout << "Running in replay mode." << std::endl;
    if (not interactive) {
      std::cout << "Running replay mode non-interactively makes no sense. "
                   "Aborted."
                << std::endl;
      return EXIT_FAILURE;
    }
    break;
  default:
    std::cout << "Unknown mode. Aborted." << std::endl;
    return EXIT_FAILURE;
  }
  
  const std::string conf_file = parse.nonOptions()[0];

  std::cout << "Configuration file: " << conf_file << std::endl;

  std::cout << "Program executed successfully." << std::endl;

  return EXIT_SUCCESS;
}
